@extends('layout')
@section('content')
    <div class="container mx-auto px-4 sm:px-8">
        <div class="py-8">
            <div>
                <h2 class="text-2xl font-semibold leading-tight">Damage Report</h2>
            </div>
            <div class="flex space-x-4 justify-end mt-5">
                <form method="POST" action="{{ url('damageReport.changeStatus/'.$damageReport->damage_report_id) }}">
                    {{ csrf_field() }}
                    <a class="py-3 px-4 inline-flex justify-center items-center rounded-md bg-blue-100 border border-transparent font-semibold text-blue-500 hover:text-white hover:bg-blue-100 focus:outline-none focus:ring-2 ring-offset-white focus:ring-blue-500 focus:ring-offset-2 transition-all text-sm dark:focus:ring-offset-gray-800" href="{{ route('damageReport.index') }}">< Back</a>

                    <button type="submit" value="approve" name="action" class="py-3 px-4 inline-flex justify-center items-center gap-2 rounded-md bg-green-100 border border-transparent font-semibold text-green-500 hover:text-white hover:bg-green-100 focus:outline-none focus:ring-2 ring-offset-white focus:ring-green-500 focus:ring-offset-2 transition-all text-sm dark:focus:ring-offset-gray-800">
                        Approved
                    </button>

                    <button type="submit" value="reject" name="action" class="py-3 px-4 inline-flex justify-center items-center gap-2 rounded-md bg-red-100 border border-transparent font-semibold text-red-500 hover:text-white hover:bg-red-100 focus:outline-none focus:ring-2 ring-offset-white focus:ring-red-500 focus:ring-offset-2 transition-all text-sm dark:focus:ring-offset-gray-800">
                        Reject
                    </button>
                </form>
            </div>
            <div class="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 overflow-x-auto">
                <div class="inline-block min-w-full shadow-md rounded-lg overflow-hidden">
                    <div class="w-full px-6 py-4 bg-white rounded shadow-md ring-1 ring-gray-900/10">
                        <h3 class="text-2xl font-semibold">{{ $damageReport->customer_name }}</h3>
                        <p class="text-base text-gray-700 mt-5">{{ $damageReport->customer_email }}</p>
                        <p class="text-base text-gray-700 mt-5">{{ $damageReport->description }}</p>


                        <section class="pt-8">
                            <div class="flex flex-wrap -mx-4">
                                @foreach ($damageReport->evidences as $evidence)
                                <div class="md:w-1/3 px-4 mb-8"><img class="rounded shadow-md" src="/images/{{ $evidence->image_name }}" alt="{{$evidence->image_name}}"></div>
                                @endforeach
                            </div>
                        </section>


                    </div>
                </div>


            </div>

            <div class="container mx-auto px-4">

            </div>

        </div>
    </div>

    <div class="container mx-auto px-4 sm:px-8">
        <div class="py-8">
            <div>
                <h2 class="text-2xl font-semibold leading-tight">Repair Shops</h2>
            </div>
            <div class="flex space-x-4 justify-end mt-5">
                <select onchange="getval(this);" id="countries" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                    <option selected value="all" {{$filter=="all"? 'selected':''}}>All</option>
                    <option value="pending" {{$filter=="pending"? 'selected':''}}>Pending</option>
                    <option value="accepted" {{$filter=="accepted"? 'selected':''}}>Accepted</option>
                    <option value="rejected" {{$filter=="rejected"? 'selected':''}}>Rejected</option>
                </select>
            </div>

            <div class="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 overflow-x-auto">
                <div class="inline-block min-w-full shadow-md rounded-lg overflow-hidden">
                    <table class="min-w-full leading-normal">
                        <thead>
                        <tr>
                            <th class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-700 uppercase tracking-wider">
                                Name
                            </th>
                            <th class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-700 uppercase tracking-wider">
                                Email
                            </th>
                            <th class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-700 uppercase tracking-wider">
                                Status
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($damageReport->repairShops as $repairShop)

                            {{--{{$repairShop->pivot->is_shop_accepted}}--}}
                            {{--{{$filter}}--}}
                            @if($repairShop->pivot->is_shop_accepted === 1 && $filter == 'accepted')
                                <tr>
                                    <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                        <p class="text-gray-900 whitespace-no-wrap">{{ $repairShop->repair_shop_name }}</p>
                                    </td>
                                    <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                        <p class="text-gray-900 whitespace-no-wrap">{{ $repairShop->repair_shop_email }}</p>
                                    </td>
                                    <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                        <span class="relative inline-block px-3 py-1 font-semibold text-green-900 leading-tight">
                                            <span aria-hidden class="absolute inset-0 bg-green-200 opacity-50 rounded-full"></span>
                                            <span class="relative">Accepted</span>
                                        </span>
                                    </td>
                                </tr>
                            @elseif($repairShop->pivot->is_shop_accepted === 0 && $filter == 'rejected')
                                <tr>
                                    <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                        <p class="text-gray-900 whitespace-no-wrap">{{ $repairShop->repair_shop_name }}</p>
                                    </td>
                                    <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                        <p class="text-gray-900 whitespace-no-wrap">{{ $repairShop->repair_shop_email }}</p>
                                    </td>
                                    <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                        <span class="relative inline-block px-3 py-1 font-semibold text-red-900 leading-tight">
                                            <span aria-hidden class="absolute inset-0 bg-red-200 opacity-50 rounded-full"></span>
                                            <span class="relative">Rejected</span>
                                        </span>
                                    </td>
                                </tr>
                            @elseif($repairShop->pivot->is_shop_accepted == NULL &&  $filter == 'pending')
                                <tr>
                                    <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                        <p class="text-gray-900 whitespace-no-wrap">{{ $repairShop->repair_shop_name }}</p>
                                    </td>
                                    <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                        <p class="text-gray-900 whitespace-no-wrap">{{ $repairShop->repair_shop_email }}</p>
                                    </td>
                                    <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                        <span class="relative inline-block px-3 py-1 font-semibold text-yellow-900 leading-tight">
                                            <span aria-hidden class="absolute inset-0 bg-yellow-200 opacity-50 rounded-full"></span>
                                            <span class="relative">Pending</span>
                                        </span>
                                    </td>
                                </tr>
                            @elseif($filter == 'all')
                                <tr>
                                    <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                        <p class="text-gray-900 whitespace-no-wrap">{{ $repairShop->repair_shop_name }}</p>
                                    </td>
                                    <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                        <p class="text-gray-900 whitespace-no-wrap">{{ $repairShop->repair_shop_email }}</p>
                                    </td>
                                    <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">

                                        @if($repairShop->pivot->is_shop_accepted === 1)
                                            <span class="relative inline-block px-3 py-1 font-semibold text-green-900 leading-tight">
                                            <span aria-hidden class="absolute inset-0 bg-green-200 opacity-50 rounded-full"></span>
                                            <span class="relative">Accepted</span>
                                        </span>
                                        @elseif($repairShop->pivot->is_shop_accepted === 0)
                                            <span class="relative inline-block px-3 py-1 font-semibold text-red-900 leading-tight">
                                            <span aria-hidden class="absolute inset-0 bg-red-200 opacity-50 rounded-full"></span>
                                            <span class="relative">Rejected</span>
                                        </span>
                                        @else
                                            <span class="relative inline-block px-3 py-1 font-semibold text-yellow-900 leading-tight">
                                            <span aria-hidden class="absolute inset-0 bg-yellow-200 opacity-50 rounded-full"></span>
                                            <span class="relative">Pending</span>
                                        </span>
                                        @endif
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        const baseUrl = "{{ url()->current() }}";
        function getval(sel)
        {
            window.location.replace(baseUrl+'?filter='+sel.value);
        }
    </script>

@endsection