<?php

namespace App\Http\Controllers;

use App\Models\RepairShop;
use Illuminate\Http\Request;

class RepairShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $repairShop = RepairShop::all();
        return view('repairShop.index',compact('repairShop'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('repairShop.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'repair_shop_email' => 'required',
            'repair_shop_name' => 'required',
            'repair_shop_latitude' => 'required',
            'repair_shop_longitude' => 'required',
        ]);

        RepairShop::create($request->all());

        return redirect()->route('repairShops.index')
            ->with('success','RepairShop created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RepairShop  $repairShop
     * @return \Illuminate\Http\Response
     */
    public function show(RepairShop $repairShop)
    {
        return view('repairShop.show',compact('repairShop'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\RepairShop  $repairShop
     * @return \Illuminate\Http\Response
     */
    public function edit(RepairShop $repairShop)
    {
        return view('repairShop.edit',compact('repairShop'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\RepairShop  $repairShop
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RepairShop $repairShop)
    {
        $request->validate([
            'repair_shop_email' => 'required',
            'repair_shop_name' => 'required',
            'repair_shop_latitude' => 'required',
            'repair_shop_longitude' => 'required',
        ]);

        $repairShop->update($request->all());

        return redirect()->route('repairShops.index')
            ->with('success','RepairShop updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RepairShop  $repairShop
     * @return \Illuminate\Http\Response
     */
    public function destroy(RepairShop $repairShop)
    {
        try {
            $repairShop->delete();
            return redirect()->route('repairShops.index')
                ->with('success','RepairShop deleted successfully');
        } catch (\Exception $e) {
//            throw new \Exception($e->getMessage());
            return back()->with('error',"This repair shop allocated some jobs, Therefore you can't delete this shop");
        }
    }
}
