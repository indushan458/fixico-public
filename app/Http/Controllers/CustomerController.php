<?php

namespace App\Http\Controllers;
use App\Models\DamageReport;
use App\Models\Evidence;
use App\Jobs\SendEmailJob;
use Illuminate\Http\Request;
use Validator;
use DB;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $customers = DamageReport::select('customer_name', 'customer_email')
                    ->distinct()
                    ->get();

        return view('customer.index',compact('customers'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DamageReport  $damageReport
     * @return \Illuminate\Http\Response
     */

    public function show(Request $request ,DamageReport $customer)
    {
        $email = \Crypt::decrypt(request()->segment(2));
        $customer = DamageReport::where('customer_email', $email)->first();
        $damageReports = DamageReport::where('customer_email',$email)->get();
        return view('customer.show',compact('customer','damageReports'));
    }

}