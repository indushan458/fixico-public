<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\DamageReport;
use App\Models\Evidence;
use Illuminate\Http\Request;
use Validator;

class DamageReportApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $damageReports = DamageReport::all();
        return response()->json([
            "success" => true,
            "message" => "Damage Reports retrieved successfully.",
            "data" => $damageReports
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'customer_name' => 'required',
            'customer_email' => 'required',
            'description' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'filename' => 'required',
            'filename.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        if($validator->fails()){
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ], 500);
        }

        $damageReport = DamageReport::create($input);

        if($request->hasfile('filename'))
        {
            foreach($request->file('filename') as $image)
            {
                $destinationPath = 'images/';
                $damageReportsImage = uniqid()."." . $image->getClientOriginalExtension();
                $image->move($destinationPath, $damageReportsImage);

                $evidence = new Evidence();
                $evidence->image_name = $damageReportsImage;
                $evidence = $damageReport->evidences()->save($evidence);
            }
        }

        return response()->json([
            "success" => true,
            "message" => "DamageReport created successfully.",
            "data" => $damageReport
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DamageReport  $damageReport
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        $damageReport = DamageReport::find($id);
        return response()->json([
            "success" => true,
            "message" => "DamageReport retrieved successfully.",
            "data" => $damageReport
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DamageReport  $damageReport
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request)
    {
        $input = $request->all();

        if($request->hasfile('filename')){
            $validator = Validator::make($input, [
                'customer_name' => 'required',
                'customer_email' => 'required',
                'description' => 'required',
                'latitude' => 'required',
                'longitude' => 'required',
                'filename' => 'required',
                'filename.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
            ]);
        }else{
            $validator = Validator::make($input, [
                'customer_name' => 'required',
                'customer_email' => 'required',
                'description' => 'required',
                'latitude' => 'required',
                'longitude' => 'required'
            ]);
        }


        if($validator->fails()){
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ], 500);
        }

        $affectedRows = DamageReport::where('damage_report_id', $input['damage_report_id'])->update(array(
            'customer_name' => $input['customer_name'],
            'customer_email' => $input['customer_email'],
            'description' => $input['description'],
            'latitude' => $input['latitude'],
            'longitude' => $input['longitude'],
        ));

        if($request->hasfile('filename'))
        {
            $images = array();
            foreach($request->file('filename') as $image)
            {
                $destinationPath = 'images/';
                $damageReportsImage = uniqid()."." . $image->getClientOriginalExtension();
                $image->move($destinationPath, $damageReportsImage);
                array_push($images,array('image_name'=>$damageReportsImage,'damage_report_id'=>$input['damage_report_id']));
            }
            Evidence::insert($images);
        }

        return response()->json([
            "success" => true,
            "message" => "Damage Report updated successfully.",
            "data" => $affectedRows
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DamageReport  $damageReport
     * @return \Illuminate\Http\Response
     */

    public function destroy(DamageReport $damageReport)
    {
        try {
            Evidence::where('damage_report_id', $damageReport->damage_report_id)->delete();
            $damageReport->delete();

            return response()->json([
                "success" => true,
                "message" => "Damage Report deleted successfully",
            ]);
        } catch (\Exception $e) {
//            throw new \Exception($e->getMessage());
            return response()->json([
                "success" => false,
                "message" => "Damage Report delete operation failed",
            ]);
        }
    }
}
