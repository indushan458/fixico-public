<?php
namespace App\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class DamageReportApproved extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * Create a new message instance.
     *
     * @return void
     */

    private $repairShops = [];

    public function __construct($data)
    {
        $this->repairShops = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */

    public function build()
    {
        $repairShops = $this->repairShops;
        return $this->view('emails.damageReportApproved',compact('repairShops'));

    }

}