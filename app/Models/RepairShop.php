<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RepairShop extends Model
{
    use HasFactory;
    protected $table = 'repair_shops';
    protected $primaryKey = 'repair_shop_id';
    protected $fillable = [
        'repair_shop_email',
        'repair_shop_name',
        'repair_shop_latitude',
        'repair_shop_longitude',
        'repair_shop_status',
    ];

    public function damageReports()
    {
        return $this->belongsToMany(DamageReport::class, 'dr_has_rs','repair_shop_id','damage_report_id')->withPivot('is_shop_accepted');
    }
}
