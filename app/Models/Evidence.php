<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Evidence extends Model
{
    use HasFactory;
    protected $table = 'evidences';
    protected $primaryKey = 'evidence_id';
    protected $fillable = [
        'image_name',
        'damage_report_id'
    ];

    /**
     * Get the DamageReport that owns the evidence.
     */

    public function damageReport()
    {
        return $this->belongsTo(DamageReport::class);
    }

}