<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
class DamageReport extends Model
{
    use HasFactory;
    protected $table = 'damage_reports';
    protected $primaryKey = 'damage_report_id';
    protected $fillable = [
        'customer_name',
        'customer_email',
        'description',
        'latitude',
        'longitude',
        'status',
    ];

    /**
     * Get the evidences for the DamageReport.
     */
    public function evidences()
    {
        return $this->hasMany(Evidence::class,'damage_report_id','damage_report_id');
    }

    /**
     * Get the repairShops for the DamageReport.
     */
    public function repairShops()
    {
        return $this->belongsToMany(RepairShop::class, 'dr_has_rs','damage_report_id','repair_shop_id')->withPivot('is_shop_accepted');
    }

    public static function getNearestRepairShops($damageReport)
    {
        $lat = $damageReport->latitude;
        $lon = $damageReport->longitude;

        #use haversine formula to get the distance

        $response = DB::table("repair_shops")
            ->select("repair_shop_name","repair_shop_email","repair_shop_id"
                ,DB::raw("6371 * acos(cos(radians(" . $lat . ")) 
                * cos(radians(repair_shops.repair_shop_latitude)) 
                * cos(radians(repair_shops.repair_shop_longitude) - radians(" . $lon . ")) 
                + sin(radians(" .$lat. ")) 
                * sin(radians(repair_shops.repair_shop_latitude))) AS distance"))
            ->having('distance', '<=', 10)
            ->orderBy('distance', 'asc')
            ->get();

        return $response;
    }

}