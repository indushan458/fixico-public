<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('repair_shops', function (Blueprint $table) {
            $table->integer('repair_shop_id', true);
            $table->string('repair_shop_email', 45)->nullable();
            $table->string('repair_shop_name', 75)->nullable();
            $table->decimal('repair_shop_latitude', 10, 7)->nullable();
            $table->decimal('repair_shop_longitude', 10, 7)->nullable();
            $table->boolean('repair_shop_status')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->dateTime('updated_at')->nullable()->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('repair_shops');
    }
};
