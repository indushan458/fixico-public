<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dr_has_rs', function (Blueprint $table) {
            $table->integer('damage_report_id')->index('fk_dr_has_rs_damage_reports1_idx');
            $table->integer('repair_shop_id')->index('fk_dr_has_rs_repair_shops1_idx');
            $table->tinyInteger('is_shop_accepted')->nullable();

            $table->primary(['damage_report_id', 'repair_shop_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dr_has_rs');
    }
};
