<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dr_has_rs', function (Blueprint $table) {
            $table->foreign(['repair_shop_id'], 'fk_repair_shops1')->references(['repair_shop_id'])->on('repair_shops')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign(['damage_report_id'], 'fk_damage_reports1')->references(['damage_report_id'])->on('damage_reports')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dr_has_rs', function (Blueprint $table) {
            $table->dropForeign('fk_repair_shops1');
            $table->dropForeign('fk_damage_reports1');
        });
    }
};
