<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DamageReportController;
use App\Http\Controllers\RepairShopController;
use App\Http\Controllers\CustomerController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
#DamageReport Routes
Route::get('/', [DamageReportController::class, 'index'])->name('damageReport.index');
Route::post('damageReport.changeStatus/{damageReport}', [DamageReportController::class, 'changeStatus'])->name('damageReport.changeStatus');
Route::post('damageReport.changeStatusByShop/{damageReport}', [DamageReportController::class, 'changeStatusByShop'])->name('damageReport.changeStatusByShop');
Route::post('damageReport.reject/{damageReport}', [DamageReportController::class, 'reject'])->name('damageReport.reject');
Route::get('damageReport/{damageReport}', [DamageReportController::class, 'show'])->name('damageReport.show');

#RepairShop Routes
Route::resource('repairShops', RepairShopController::class);

#Customer Routes
Route::resource('customers', CustomerController::class);
Route::get('customersShow/{email?}', [CustomerController::class, 'show'])->name('customersShow');
