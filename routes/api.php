<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\Api\DamageReportApiController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::controller(DamageReportApiController::class)->group(function(){
    Route::get('damageReports', 'index')->name('damageReport.index');
    Route::post('damageReport', 'store')->name('damageReport.store');
    Route::get('damageReports/{damageReport}', 'show')->name('damageReport.show');
    Route::post('damageReportsUpdate', 'update')->name('damageReport.update');
    Route::delete('damageReports/{damageReport}', 'destroy')->name('damageReport.destroy');
});